/**
 * @author Oman Fathurohman <oman21.dev@gmail.com>
 */

import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { BrandColor } from './src/components/Color';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { createStackNavigator } from '@react-navigation/stack';
import { AnimatedTabBarNavigator } from "react-native-animated-nav-tab-bar";

import HomeStack from './src/routes/HomeStack';
import BlankStack from './src/routes/BlankStack';

const Stack = createStackNavigator();
Stack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

const Tabs = AnimatedTabBarNavigator()

function MainMenu() {
  return(
    <Tabs.Navigator
      initialRouteName="Beranda"
      tabBarOptions={{
        keyboardHidesTabBar: true,
        tabStyle:{
          elevation: 14,
          fontFamily: 'Nunito-Regular',
          borderTopLeftRadius:25,
          borderTopRightRadius:25
        },
        activeTintColor: "#fff",
        inactiveTintColor: "#222222"
      }}
      appearence={{
        activeTabBackgrounds: BrandColor()
      }}
    >
      <Tabs.Screen
        name="Home"
        component={HomeStack}
        options={(route) => ({
          tabBarIcon: ({ focused, color }) => (
            <Feather
              focused={focused}
              color={color}
              size={20}
              name="home"
            />
          ),
          tabBarVisible: getTabBarVisibility(route),
        })}
      /> 
      <Tabs.Screen
        name="Discover"
        component={BlankStack}
        options={{
          tabBarIcon: ({ focused, color }) => (
            <Feather
              focused={focused}
              color={color}
              size={20}
              name="search"
            />
          ),
        }}
      /> 
      <Tabs.Screen
        name="Ticket"
        component={BlankStack}
        options={{
          tabBarIcon: ({ focused, color }) => (
            <FontAwesome
              focused={focused}
              color={color}
              size={20}
              name="ticket"
            />
          ),
        }}
      />
      <Tabs.Screen
        name="Account"
        component={BlankStack}
        options={{
          tabBarIcon: ({ focused, color }) => (
            <Feather
              focused={focused}
              color={color}
              size={20}
              name="user"
            />
          ),
        }}
      /> 
    </Tabs.Navigator>
  )
}

const App = () => {
  
  useEffect(() => {
    // setTimeout( () => {
    //   SplashScreen.hide();
    // }, 1000);

  }, [] );

  return(
    <NavigationContainer>
      <Stack.Navigator
      initialRouteName={MainMenu}
      screenOptions={{
        headerShown: false
      }}>
        <Stack.Screen
          name="MainMenu"
          component={MainMenu}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

function getTabBarVisibility(route) {
	const index = route.route.state
		? route.route.state.index
    : 0;
	if ( index > 0 ) {
		return false;
	}

	return true;
};

export default App
