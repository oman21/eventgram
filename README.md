# Eventgram

Sample untuk aplikasi menggunakan React Native dengan design saya sendiri.

![screenshots of example app](/images/preview.png)

## Install dependencies
```
git clone https://gitlab.com/oman21/eventgram.git
cd eventgram
npm install
```

### Author

Oman Fathurohman
oman21.dev@gmail.com