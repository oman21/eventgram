import React from 'react';
import { SafeAreaView, View } from 'react-native';
import { EventTextBold, EventText } from './../components/Theme';
import { BrandColor } from './../components/Color';
import { Header } from 'react-native-elements';
import Feather from 'react-native-vector-icons/Feather';

const Blank = ({ navigation }) => {
  
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Header
        placement="center"
				centerComponent={()=><EventTextBold style={{fontSize:18, textAlign:'center', alignSelf: 'center', color: BrandColor()}}>Under Constructor</EventTextBold>}
        containerStyle={{borderBottomWidth:1, backgroundColor:'#fff', borderBottomColor: '#ccc'}}
      />

			<View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
				<EventTextBold style={{fontSize:20}}>Oman Fathurohman</EventTextBold>
				<View style={{marginTop:10, justifyContent:'center', alignItems:'center', flexDirection:'row'}}>
					<Feather name={'mail'} color={BrandColor()} size={20}/>
					<EventText style={{marginLeft:10, fontSize:16}}>oman21.dev@gmail.com</EventText>
				</View>
				<View style={{marginTop:10, justifyContent:'center', alignItems:'center', flexDirection:'row'}}>
					<Feather name={'phone'} color={BrandColor()} size={20}/>
					<EventText style={{marginLeft:10, fontSize:16}}>0812 8120 2072</EventText>
				</View>
			</View>
    </SafeAreaView>
  );
}

export default Blank;