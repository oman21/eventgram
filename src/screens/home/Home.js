import React, { useState } from 'react';
import { SafeAreaView, View, ScrollView, TouchableOpacity, ImageBackground, Dimensions, StatusBar, Image } from 'react-native';
import { BrandColor } from './../../components/Color';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { EventText, EventTextBold } from './../../components/Theme';
import Carousel from 'react-native-snap-carousel';

const win = Dimensions.get('window');

const Home = ({ navigation  }) => {

	const [slider, setSlider] = useState(
		[
			{
					image:"https://vignette.wikia.nocookie.net/linkin-park-junior-underground/images/d/d3/FortMinorMyspace.JPG/revision/latest/scale-to-width-down/340?cb=20141016182246",
					text: "Mike Shinoda will be at Jakarta at 1 Januari 2021",
			},
			{
					image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT2wfldilY_PKGpLf2Gkqpe05hyE-jwEILBKQ&usqp=CAU",
					text: "Unique Corporate Event Ideas That Will Amaze",
			},
			{
					image:"https://static.billboard.com/files/media/01-Linkin-Park-oct-27-2017-billboard-1548-compressed.jpg",
					text: "Linkin Park Livestreaming Unseen 'Hybrid Theory'",
			},
			{
				image:"https://vignette.wikia.nocookie.net/linkin-park-junior-underground/images/d/d3/FortMinorMyspace.JPG/revision/latest/scale-to-width-down/340?cb=20141016182246",
				text: "Mike Shinoda will be at Jakarta at 1 Januari 2021",
		},
		{
				image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT2wfldilY_PKGpLf2Gkqpe05hyE-jwEILBKQ&usqp=CAU",
				text: "Unique Corporate Event Ideas That Will Amaze",
		},
		{
				image:"https://static.billboard.com/files/media/01-Linkin-Park-oct-27-2017-billboard-1548-compressed.jpg",
				text: "Linkin Park Livestreaming Unseen 'Hybrid Theory'",
		}
		]
	);

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor:'#eeeff3' }}>
      <StatusBar backgroundColor={'#000'} barStyle="light-content"/>

			<ScrollView showsVerticalScrollIndicator={false}>

				<ImageBackground source={require('./../../assets/images/bg.png')} resizeMode={'cover'} style={{paddingTop: StatusBar.currentHeight}}>
					<View style={{padding:15, flexDirection:'row', alignItems:'center'}}>
						<View style={{flex:1}}>
							<EventText style={{color:'#fff'}}>Today, 12 December 2020</EventText>
							<EventTextBold style={{color:'#fff', fontSize:24}}>Discover Events</EventTextBold>
						</View>
						<TouchableOpacity style={{width:24, marginRight:30}}>
							<Feather name={'search'} size={24} color={'#fff'}/>
						</TouchableOpacity>
						<TouchableOpacity style={{width:24}}>
							<Feather name={'bell'} size={24} color={'#fff'}/>
							<View style={{width:20, height:20, backgroundColor:'red', borderRadius:50, position:'absolute', top:-10, right:-5, flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
								<EventText style={{color:'#fff', fontSize:10}}>21</EventText>
							</View>
						</TouchableOpacity>
					</View>

					<View style={{marginTop:15}}/>

					<Carousel
						data={slider}
						renderItem={renderSlider}
						hasParallaxImages={true}
						sliderWidth={win.width}
						itemWidth={win.width-180}
						layout={'default'}
						autoplay={true}
						autoplayInterval={5000}
						loop={true}
						inactiveSlideOpacity={1}
						activeSlideAlignment={'center'}
						inactiveSlideScale={0.8}
					/>
				</ImageBackground>

				<ScrollView showsHorizontalScrollIndicator={false} horizontal>
					<View style={{flexDirection:'row', marginTop:30}}>
						<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, flexDirection:'row', paddingHorizontal:15, paddingVertical:10, marginLeft:15}}>
							<Feather name={'archive'} size={20} color={'#009688'}/>
							<EventTextBold style={{flex:1, paddingLeft: 10, fontSize:16}}>Music</EventTextBold>
						</TouchableOpacity>
						<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, flexDirection:'row', paddingHorizontal:15, paddingVertical:10, marginLeft:15}}>
							<Feather name={'archive'} size={20} color={'#9c27b0'}/>
							<EventTextBold style={{flex:1, paddingLeft: 10, fontSize:16}}>Exhibitions</EventTextBold>
						</TouchableOpacity>
						<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, flexDirection:'row', paddingHorizontal:15, paddingVertical:10, marginLeft:15}}>
							<Feather name={'archive'} size={20} color={'#f44336'}/>
							<EventTextBold style={{flex:1, paddingLeft: 10, fontSize:16}}>Festivals</EventTextBold>
						</TouchableOpacity>
						<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, flexDirection:'row', paddingHorizontal:15, paddingVertical:10, marginLeft:15, marginRight:20}}>
							<Feather name={'archive'} size={20} color={'#ffc107'}/>
							<EventTextBold style={{flex:1, paddingLeft: 10, fontSize:16}}>Tech</EventTextBold>
						</TouchableOpacity>
					</View>
				</ScrollView>

				<View style={{padding:15, marginTop:15, flexDirection:'row', alignItems:'center'}}>
					<EventTextBold style={{fontSize:18, color: '#000', flex: 1}}>Top Trending</EventTextBold>
					<TouchableOpacity><EventTextBold style={{color:BrandColor()}}>View All</EventTextBold></TouchableOpacity>
				</View>

				<ScrollView showsHorizontalScrollIndicator={false} horizontal>
					<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, width:240, marginLeft:15, overflow:'hidden'}} onPress={()=>navigation.navigate('Detail')}>
						<Image 
							source={{uri:"https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F120920949%2F20763473980%2F1%2Foriginal.20201214-185117?w=1080&auto=format%2Ccompress&q=75&sharp=10&rect=0%2C0%2C2160%2C1080&s=d2931d2e29d3eebac89c8fb8ff7cf873"}}
							style={{width:240, height:150, resizeMode:'cover'}}
						/>
						<View style={{flexDirection:'row', padding:10}}>
							<View style={{width:40, height:40, backgroundColor:'#ff5722', borderRadius:10, flexDirection:'column', justifyContent:'center', alignItems:'center', padding:4}}>
								<EventTextBold style={{color:'#fff'}}>26</EventTextBold>
								<EventText style={{color:'#fff', fontSize:10, marginTop:-4}}>Des</EventText>
							</View>
							<View style={{flex:1, paddingLeft:6}}>
								<EventTextBold numberOfLines={2}>The Mads: Santa Claus vs. The Devil - Live riffing with MST3K's The Mads!</EventTextBold>
								<View style={{flexDirection:'row', alignItems:'center'}}>
									<FontAwesome name={'map-marker'} size={10} color={'#5f646c'}/>
									<EventText style={{flex:1, paddingLeft:6, fontSize:12, color:'#5f646c'}} numberOfLines={1}>Gelora Bung Karno, Jakarta</EventText>
								</View>
							</View>
						</View>
					</TouchableOpacity>
					<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, width:240, marginLeft:15, overflow:'hidden'}} onPress={()=>navigation.navigate('Detail')}>
						<Image 
							source={{uri:"https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F102066008%2F185750211993%2F1%2Foriginal.20200528-112745?w=1080&auto=format%2Ccompress&q=75&sharp=10&rect=72%2C0%2C2052%2C1026&s=3079bb3b89361bbfc2fa37458b4eb8a8"}}
							style={{width:240, height:150, resizeMode:'cover'}}
						/>
						<View style={{flexDirection:'row', padding:10}}>
							<View style={{width:40, height:40, backgroundColor:'#ff5722', borderRadius:10, flexDirection:'column', justifyContent:'center', alignItems:'center', padding:4}}>
								<EventTextBold style={{color:'#fff'}}>1</EventTextBold>
								<EventText style={{color:'#fff', fontSize:10, marginTop:-4}}>March</EventText>
							</View>
							<View style={{flex:1, paddingLeft:6}}>
								<EventTextBold numberOfLines={2}>A FEMINIST'S GUIDE TO BOTANY: Online Botanical Painting Session</EventTextBold>
								<View style={{flexDirection:'row', alignItems:'center'}}>
									<FontAwesome name={'map-marker'} size={10} color={'#5f646c'}/>
									<EventText style={{flex:1, paddingLeft:6, fontSize:12, color:'#5f646c'}} numberOfLines={1}>Gelora Bung Karno, Jakarta</EventText>
								</View>
							</View>
						</View>
					</TouchableOpacity>
					<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, width:240, marginLeft:15, marginRight:15, overflow:'hidden'}} onPress={()=>navigation.navigate('Detail')}>
						<Image 
							source={{uri:"https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F119372695%2F17810699237%2F1%2Foriginal.20201202-181953?w=1080&auto=format%2Ccompress&q=75&sharp=10&rect=0%2C0%2C2160%2C1080&s=f8341089fa529d8e9e69807af6445aa9"}}
							style={{width:240, height:150, resizeMode:'cover'}}
						/>
						<View style={{flexDirection:'row', padding:10}}>
							<View style={{width:40, height:40, backgroundColor:'#ff5722', borderRadius:10, flexDirection:'column', justifyContent:'center', alignItems:'center', padding:4}}>
								<EventTextBold style={{color:'#fff'}}>1</EventTextBold>
								<EventText style={{color:'#fff', fontSize:10, marginTop:-4}}>March</EventText>
							</View>
							<View style={{flex:1, paddingLeft:6}}>
								<EventTextBold numberOfLines={2}>Master The Trade: Master Your Money and Mindset For Investing in 2021</EventTextBold>
								<View style={{flexDirection:'row', alignItems:'center'}}>
									<FontAwesome name={'map-marker'} size={10} color={'#5f646c'}/>
									<EventText style={{flex:1, paddingLeft:6, fontSize:12, color:'#5f646c'}} numberOfLines={1}>Gelora Bung Karno, Jakarta</EventText>
								</View>
							</View>
						</View>
					</TouchableOpacity>
				</ScrollView>


				<View style={{padding:15, marginTop:15, flexDirection:'row', alignItems:'center'}}>
					<EventTextBold style={{fontSize:18, color: '#000', flex: 1}}>Popular Organizer</EventTextBold>
					<TouchableOpacity><EventTextBold style={{color:BrandColor()}}>View All</EventTextBold></TouchableOpacity>
				</View>

				<ScrollView showsHorizontalScrollIndicator={false} horizontal>
					<View style={{flexDirection:'row'}}>
						<TouchableOpacity style={{padding:10, marginLeft:15, flexDirection:'column', alignItems:'center', width:100}}>
							<Image source={{uri:'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F17192533%2F32014549035%2F2%2Foriginal.jpg?h=200&w=200&auto=format%2Ccompress&q=75&sharp=10&s=3dc583e0a78e106aca2c366cae88d106'}} style={{width:60, height:60, resizeMode:'cover', borderRadius:50}}/>
							<EventTextBold style={{marginTop:5}} numberOfLines={1}>Canada Job Expo</EventTextBold>
							<EventText style={{fontSize:10, color:'#5f646c'}}>4774 follower</EventText>
						</TouchableOpacity>
						<TouchableOpacity style={{padding:10, marginLeft:15, flexDirection:'column', alignItems:'center', width:100}}>
							<Image source={{uri:'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F106145376%2F12602525447%2F2%2Foriginal.jpg?h=200&w=200&auto=format%2Ccompress&q=75&sharp=10&s=53afae579aad7b9449b3ca554b85eec2'}} style={{width:60, height:60, resizeMode:'cover', borderRadius:50}}/>
							<EventTextBold style={{marginTop:5}} numberOfLines={1}>Rock Star Beer Festivals</EventTextBold>
							<EventText style={{fontSize:10, color:'#5f646c'}}>7761 follower</EventText>
						</TouchableOpacity>
						<TouchableOpacity style={{padding:10, marginLeft:15, flexDirection:'column', alignItems:'center', width:100}}>
							<Image source={{uri:'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F43658704%2F84203646226%2F2%2Foriginal.jpg?h=200&w=200&auto=format%2Ccompress&q=75&sharp=10&s=a8f3ccdae7f7995f8bd2b9567d0b373d'}} style={{width:60, height:60, resizeMode:'cover', borderRadius:50}}/>
							<EventTextBold style={{marginTop:5}} numberOfLines={1}>ThingstodoDC.com</EventTextBold>
							<EventText style={{fontSize:10, color:'#5f646c'}}>2732 follower</EventText>
						</TouchableOpacity>
						<TouchableOpacity style={{padding:10, marginLeft:15, flexDirection:'column', alignItems:'center', width:100}}>
							<Image source={{uri:'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F115585637%2F156754398215%2F1%2Foriginal.20201022-233935?h=200&w=200&auto=format%2Ccompress&q=75&sharp=10&rect=0%2C0%2C240%2C240&s=7e666fbb040f8c7a966d1026be39bfbf'}} style={{width:60, height:60, resizeMode:'cover', borderRadius:50}}/>
							<EventTextBold style={{marginTop:5}} numberOfLines={1}>Healthy Little Cook</EventTextBold>
							<EventText style={{fontSize:10, color:'#5f646c'}}>2108 follower</EventText>
						</TouchableOpacity>
					</View>
				</ScrollView>

				<View style={{padding:15, marginTop:15, flexDirection:'row', alignItems:'center'}}>
					<EventTextBold style={{fontSize:18, color: '#000', flex: 1}}>Today Happening</EventTextBold>
					<TouchableOpacity><EventTextBold style={{color:BrandColor()}}>View All</EventTextBold></TouchableOpacity>
				</View>
				<View style={{paddingHorizontal:15}}>
					<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, overflow: 'hidden', marginBottom:15}}>
						<Image 
							source={{uri:"https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F120920949%2F20763473980%2F1%2Foriginal.20201214-185117?w=1080&auto=format%2Ccompress&q=75&sharp=10&rect=0%2C0%2C2160%2C1080&s=d2931d2e29d3eebac89c8fb8ff7cf873"}}
							style={{width: win.width-30, height: win.width-220, resizeMode:'contain'}}
						/>
						<View style={{flexDirection:'row', padding:10, alignItems:'center'}}>
							<Image source={{uri:'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F106145376%2F12602525447%2F2%2Foriginal.jpg?h=200&w=200&auto=format%2Ccompress&q=75&sharp=10&s=53afae579aad7b9449b3ca554b85eec2'}} style={{width:40, height:40, resizeMode:'cover', borderRadius:50}}/>
							<View style={{flex:1, paddingLeft:10}}>
								<EventTextBold numberOfLines={2}>Rock Star Beer Festivals</EventTextBold>
								<View style={{flexDirection:'row', alignItems:'center'}}>
									<FontAwesome name={'map-marker'} size={12} color={'#5f646c'}/>
									<EventText style={{flex:1, paddingLeft:6, fontSize:10, color:'#5f646c'}}>Gelora Bung Karno, Jakarta</EventText>
								</View>
							</View>
						</View>
						<View style={{padding:10}}>
							<EventTextBold>The Mads: Santa Claus vs. The Devil - Live riffing with MST3K's The Mads!</EventTextBold>
							<EventTextBold style={{color:'#ff5722', fontSize:13, marginTop:6}}>Wed, Dec 16 08:00 GMT+07:00</EventTextBold>
							<View style={{flexDirection:'row', marginTop:10}}>
								<View style={{flex:1, flexDirection:'row'}}>
									<TouchableOpacity style={{borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
										<MaterialCommunityIcons name={'human-greeting'} size={20} color={'#e91e63'}/>
										<EventText style={{marginLeft:5}}>604 will join</EventText>
									</TouchableOpacity>
									<TouchableOpacity style={{marginLeft:10, borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
										<MaterialCommunityIcons name={'chat'} size={20} color={'#607d8b'}/>
										<EventText style={{marginLeft:8}}>28</EventText>
									</TouchableOpacity>
								</View>
								<TouchableOpacity style={{borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
									<FontAwesome name={'share'} size={20} color={'#607d8b'}/>
								</TouchableOpacity>
							</View>
						</View>
					</TouchableOpacity>
					<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, overflow: 'hidden', marginBottom:15}}>
						<Image 
							source={{uri:"https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F103311548%2F199607744421%2F1%2Foriginal.20200611-143536?w=1080&auto=format%2Ccompress&q=75&sharp=10&rect=0%2C0%2C2160%2C1080&s=ddd44cf84adae76339e6ef4266fffc56"}}
							style={{width: win.width-30, height: win.width-220, resizeMode:'contain'}}
						/>
						<View style={{flexDirection:'row', padding:10, alignItems:'center'}}>
							<Image source={{uri:'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F43658704%2F84203646226%2F2%2Foriginal.jpg?h=200&w=200&auto=format%2Ccompress&q=75&sharp=10&s=a8f3ccdae7f7995f8bd2b9567d0b373d'}} style={{width:40, height:40, resizeMode:'cover', borderRadius:50}}/>
							<View style={{flex:1, paddingLeft:10}}>
								<EventTextBold numberOfLines={2}>ThingstodoDot.com</EventTextBold>
								<View style={{flexDirection:'row', alignItems:'center'}}>
									<FontAwesome name={'map-marker'} size={12} color={'#5f646c'}/>
									<EventText style={{flex:1, paddingLeft:6, fontSize:10, color:'#5f646c'}}>Gelora Bung Karno, Jakarta</EventText>
								</View>
							</View>
						</View>
						<View style={{padding:10}}>
							<EventTextBold>Baking with Our Buds - Open House Online!</EventTextBold>
							<EventTextBold style={{color:'#ff5722', fontSize:13, marginTop:6}}>Wed, Dec 16 08:00 GMT+07:00</EventTextBold>
							<View style={{flexDirection:'row', marginTop:10}}>
								<View style={{flex:1, flexDirection:'row'}}>
									<TouchableOpacity style={{borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
										<MaterialCommunityIcons name={'human-greeting'} size={20} color={'#607d8b'}/>
										<EventText style={{marginLeft:5}}>340 will join</EventText>
									</TouchableOpacity>
									<TouchableOpacity style={{marginLeft:10, borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
										<MaterialCommunityIcons name={'chat'} size={20} color={'#607d8b'}/>
										<EventText style={{marginLeft:8}}>20</EventText>
									</TouchableOpacity>
									<View style={{marginLeft:10, borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
										<EventText>Free</EventText>
									</View>
								</View>
								<TouchableOpacity style={{borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
									<FontAwesome name={'share'} size={20} color={'#607d8b'}/>
								</TouchableOpacity>
							</View>
						</View>
					</TouchableOpacity>
					<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, overflow: 'hidden', marginBottom:15}}>
						<Image 
							source={{uri:"https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F113878383%2F145991937686%2F1%2Foriginal.20201007-105953?w=1080&auto=format%2Ccompress&q=75&sharp=10&rect=0%2C45%2C1168%2C584&s=15f9e052c87fab41d5da4156e84dad2b"}}
							style={{width: win.width-30, height: win.width-220, resizeMode:'contain'}}
						/>
						<View style={{flexDirection:'row', padding:10, alignItems:'center'}}>
							<Image source={{uri:'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F43658704%2F84203646226%2F2%2Foriginal.jpg?h=200&w=200&auto=format%2Ccompress&q=75&sharp=10&s=a8f3ccdae7f7995f8bd2b9567d0b373d'}} style={{width:40, height:40, resizeMode:'cover', borderRadius:50}}/>
							<View style={{flex:1, paddingLeft:10}}>
								<EventTextBold numberOfLines={2}>ThingstodoDot.com</EventTextBold>
								<View style={{flexDirection:'row', alignItems:'center'}}>
									<FontAwesome name={'map-marker'} size={12} color={'#5f646c'}/>
									<EventText style={{flex:1, paddingLeft:6, fontSize:10, color:'#5f646c'}}>Gelora Bung Karno, Jakarta</EventText>
								</View>
							</View>
						</View>
						<View style={{padding:10}}>
							<EventTextBold>#ACT100: Mindfulness and Acceptance for Self-Esteem</EventTextBold>
							<EventTextBold style={{color:'#ff5722', fontSize:13, marginTop:6}}>Wed, Dec 16 08:00 GMT+07:00</EventTextBold>
							<View style={{flexDirection:'row', marginTop:10}}>
								<View style={{flex:1, flexDirection:'row'}}>
									<TouchableOpacity style={{borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
										<MaterialCommunityIcons name={'human-greeting'} size={20} color={'#607d8b'}/>
									</TouchableOpacity>
									<TouchableOpacity style={{marginLeft:10, borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
										<MaterialCommunityIcons name={'chat'} size={20} color={'#607d8b'}/>
									</TouchableOpacity>
								</View>
								<TouchableOpacity style={{borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
									<FontAwesome name={'share'} size={20} color={'#607d8b'}/>
								</TouchableOpacity>
							</View>
						</View>
					</TouchableOpacity>
				</View>
				<View style={{height:15}}/>
			</ScrollView>
    </SafeAreaView>
	);
	
	function renderSlider({item,index}){
		return (
			<View style={{borderRadius: 15, overflow: 'hidden'}}>
				<ImageBackground 
					source={{uri: item.image}}
					resizeMode={"cover"}
					style={{
						backgroundColor:'floralwhite',
						width: win.width-180,
						height: win.width-180
					}}
				>
					<View style={{flex:1, backgroundColor: '#66207259', padding:15, flexDirection:'column', justifyContent:'flex-end'}}>
						<EventTextBold style={{color:'#fff', fontSize:16}}>{item.text}</EventTextBold>
					</View>
					
				</ImageBackground>
			</View>
		)
	}
}

export default Home;