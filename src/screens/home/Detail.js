import React, { useState } from 'react';
import { 
	View,
	Image,
	Dimensions,
	StatusBar,
	TouchableOpacity
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { EventText, EventTextBold } from './../../components/Theme';
import { BrandColor } from './../../components/Color';
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import HTML from 'react-native-render-html';
import { Button } from 'react-native-elements';

const MIN_HEIGHT = 80;
const MAX_HEIGHT = 220;

const Detail = ({ navigation }) => {

	const [showTitle, setShowTitle] = useState(false);

	const fadeIn = {
		from: {
			opacity: 0,
		},
		to: {
			opacity: 1,
		},
	};

	const fadeOut = {
		from: {
			opacity: 0,
		},
		to: {
			opacity: 0,
		},
	};
  
  return (
    <View style={{ flex: 1, backgroundColor:'#fff' }}>
			<StatusBar
				translucent
				barStyle="light-content"
				backgroundColor="rgba(0, 0, 0, 0)"
			/>
			
			<HeaderImageScrollView
				maxHeight={MAX_HEIGHT}
				minHeight={MIN_HEIGHT}
				maxOverlayOpacity={0.6}
				minOverlayOpacity={0.3}
				fadeOutForeground
				renderHeader={() => <Image source={{uri:'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F120920949%2F20763473980%2F1%2Foriginal.20201214-185117?w=1080&auto=format%2Ccompress&q=75&sharp=10&rect=0%2C0%2C2160%2C1080&s=d2931d2e29d3eebac89c8fb8ff7cf873'}} style={{height: MAX_HEIGHT,width: Dimensions.get('window').width,alignSelf: 'stretch',resizeMode: 'cover'}} />}
				renderFixedForeground={() => (
					<View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', padding:15}}>
						<TouchableOpacity style={{width:50}} onPress={()=>navigation.goBack()}>
							<Feather name="chevron-left" size={28} color={'white'}/>
						</TouchableOpacity>
						<Animatable.View
							style={{
								flex:1,
								height: MIN_HEIGHT,
								paddingTop: StatusBar.currentHeight+5
							}}
							animation={showTitle?fadeIn:fadeOut}
						>
							<EventTextBold numberOfLines={1} style={{color:'#fff', fontSize:18, marginTop:-4}}>The Mads: Santa Claus vs. The Devil - Live riffing with MST3K's The Mads!</EventTextBold>
						</Animatable.View>
					</View>
				)}
			>
				<View
					style={{padding: 15, borderBottomWidth: 1, borderBottomColor: '#eee', backgroundColor: 'white'}}
				>
					<EventTextBold style={{fontSize:18}}>The Mads: Santa Claus vs. The Devil - Live riffing with MST3K's The Mads!</EventTextBold>
					<TriggeringView style={{position:'absolute', top:-60}} onHide={() => setShowTitle(true)} onDisplay={() => setShowTitle(false)}/>
					<View style={{flexDirection:'row', marginTop:10}}>
						<View style={{flex:1, flexDirection:'row'}}>
							<TouchableOpacity style={{borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
								<MaterialCommunityIcons name={'human-greeting'} size={20} color={'#e91e63'}/>
								<EventText style={{marginLeft:5}}>604 will join</EventText>
							</TouchableOpacity>
							<TouchableOpacity style={{marginLeft:10, borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
								<MaterialCommunityIcons name={'chat'} size={20} color={'#607d8b'}/>
								<EventText style={{marginLeft:8}}>28</EventText>
							</TouchableOpacity>
						</View>
						<TouchableOpacity style={{borderRadius:50, backgroundColor:'#fff', elevation:4, padding:8, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
							<FontAwesome name={'share'} size={20} color={'#607d8b'}/>
						</TouchableOpacity>
					</View>
				</View>
				<View style={{flexDirection:'row', alignItems:'center', padding:15}}>
					<Image source={{uri:'https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F106145376%2F12602525447%2F2%2Foriginal.jpg?h=200&w=200&auto=format%2Ccompress&q=75&sharp=10&s=53afae579aad7b9449b3ca554b85eec2'}} style={{width:40, height:40, resizeMode:'cover', borderRadius:50}}/>
					<View style={{flex:1, paddingLeft:10}}>
						<EventTextBold numberOfLines={2}>Rock Star Beer Festivals</EventTextBold>
						<EventText>Followed</EventText>
					</View>
				</View>
				<View style={{padding:15, borderBottomWidth: 1, borderBottomColor: '#eee'}}>
					<View style={{flexDirection:'row'}}>
						<Feather name={'calendar'} size={20} color={'#607d8b'}/>
						<View style={{flex:1, marginLeft:15}}>
							<EventTextBold>Thursday, December 17</EventTextBold>
							<EventText>00:00 - 01:00 GMT+07:00</EventText>
						</View>
					</View>
					<View style={{flexDirection:'row', marginTop:10}}>
						<Feather name={'airplay'} size={20} color={'#607d8b'}/>
						<View style={{flex:1, marginLeft:15}}>
							<EventTextBold>Online Event</EventTextBold>
						</View>
					</View>
					<View style={{flexDirection:'row', marginTop:10}}>
						<Feather name={'clock'} size={20} color={'#607d8b'}/>
						<View style={{flex:1, marginLeft:15}}>
							<EventTextBold>Duration: 1 hour</EventTextBold>
						</View>
					</View>
				</View>
				<View style={{padding:15}}>
					<EventTextBold style={{fontSize:18}}>About this Event</EventTextBold>
					<HTML html={
						'<p style="text-align:justify">The Mads are back! Join&nbsp;<strong><em>Mystery Science Theater 3000</em></strong>&nbsp;stars&nbsp;<strong>Trace Beaulieu&nbsp;</strong>and&nbsp;<strong>Frank Conniff</strong>&nbsp;(&ldquo;Dr. Clayton Forrester&rdquo; and &ldquo;TV&rsquo;s Frank&rdquo;) for this special holiday livestream event as they live-riff through the bizarre 1959 Mexican fantasy film,&nbsp;<strong>SANTA CLAUS VS. THE DEVIL</strong>, with a Q&amp;A with Frank &amp; Trace and very special guests&nbsp;<strong>Andy Kindler</strong>&nbsp;(<em>Bob&#39;s Burgers</em>,&nbsp;<em>Maron</em>) and&nbsp;<strong>J. Elvis Weinstein</strong>&nbsp;(<em>MST3K</em>,&nbsp;<em>Freaks and Geeks</em>)!</p><p style="text-align:justify">This show will broadcast live via a private Twitch link that will be sent to ticket purchasers an hour prior to showtime.</p><p style="text-align:justify"><strong>Viewers will be able to tune in from anywhere in the world.</strong></p><p style="text-align:justify">A portion of the proceeds from this event will benefit&nbsp;<a href="https://www.finfood.org/" rel="nofollow noopener noreferrer" target="_blank">Friends In Need Food Shelf</a>&nbsp;in Minneapolis.</p><h2 style="text-align:justify">Anyone who purchases a ticket will gain access to re-watch the livestream and download it for personal viewing. Please allow 24 hours for us to process the video and make it available for rewatching and downloading.</h2><h2 style="text-align:justify"><em>The show will not be available for purchase after the broadcast ends.</em></h2><p style="text-align:justify"><strong>SANTA CLAUS VS. THE DEVIL</strong>&nbsp;(1959) (<a href="https://en.wikipedia.org/wiki/Santa_Claus_(1959_film)" rel="nofollow noopener noreferrer" target="_blank">taken from Wikipedia</a>)</p><p style="text-align:justify"><em>Santa Claus</em>&nbsp;(sometimes also known as&nbsp;<em>Santa Claus vs. the Devil</em>) is a 1959 Mexican fantasy film directed by Rene Cardona and co-written with Adolfo Torres Portillo. In the film, Santa works in outer space and does battle with a demon named Pitch, sent to Earth by Lucifer to ruin Christmas by killing Santa and &quot;making all the children of the Earth do evil&quot;.</p><p style="text-align:justify">A dubbed and slightly edited English-language version was produced for U.S. release in 1960 under the direction of Ken Smith. It was lampooned on an episode of&nbsp;<em>Mystery Science Theater 3000</em>.</p><p style="text-align:justify">The film was considered to be a financial success over several holiday season theatrical releases in the 1960s and 1970s. Broadcast of the film also became a holiday tradition at several U.S. television stations. The film garnered at least one award, winning the Golden Gate Award for Best International Family Film at the San Francisco International Film Festival in 1959.</p><p style="text-align:justify">The film was also featured in the fifth season of Mystery Science Theater 3000 (episode #521), which first aired on Christmas Eve 1993. The devil Pitch became a recurring character on MST3K, played by writer Paul Chaplin. Chaplin said, &quot;It&#39;s kind of a fun movie, and we all enjoyed it&quot;, noting that he made &quot;quite an attractive demon&quot;.[4] The episode may be the origin of the phrase &quot;nightmare fuel&quot;; during the movie, Crow T. Robot calls an animatronic Santa &quot;some good old-fashioned nightmare fuel&quot;, and the phrase spread through MST3K message boards.[5]</p><p style="text-align:justify"><strong>Frank Conniff</strong>&nbsp;is a comedy writer and performer who began his TV career writing for the Peabody award-winning series&nbsp;<em>Mystery Science Theater 3000</em>, where he also played &ldquo;TV&rsquo;s Frank,&rdquo; the bumbling yet lovable mad scientist sidekick. He then went on to be a writer, producer and actor on the ABC TV series&nbsp;<em>Sabrina the Teenage Witch</em>, and he was a writer and producer on&nbsp;<em>The Drew Carey Show</em>&nbsp;on ABC,&nbsp;<em>The New Tom Green Show</em>&nbsp;on MTV, and the satirical series&nbsp;<em>O2BE</em>&nbsp;on the Oxygen Network, where he was also a cast member. He was head writer of the animated Nickelodeon series,&nbsp;<em>Invader Zim</em>, and also was a writer and producer for the Air America Radio network, where he provided material for on-air personalities Rachel Maddow, Marc Maron, Lizz Winstead, Al Franken, Janeane Garofalo, and Randi Rhodes. He was a writer and performer on&nbsp;<em>Viewpoint</em>&nbsp;with John Fugelsang on Current TV, and on&nbsp;<em>Totally Biased with W. Kamau Bell&nbsp;</em>on the FXX network, and he also writes and performs on&nbsp;<em>The Jimmy Dore Show</em>&nbsp;on Pacifica Radio.</p><p style="text-align:justify"><strong>Trace Beaulieu</strong>&nbsp;is a comic actor, writer, performer. He was a founding member of the Emmy- nominated, Peabody Award-winning, cult hit show&nbsp;<em>Mystery Science Theater 3000</em>. In addition to writing, occasionally directing, designing and building sets and props, Trace performed the puppet character Crow T. Robot, and mad scientist Dr. Clayton Forrester. He repeated these roles in the 1996 film version of the show, MST3K: The Movie. Trace wrote for ABC&rsquo;s&nbsp;<em>America&rsquo;s Funniest Videos</em>&nbsp;for nine seasons. He played a recurring character on&nbsp;<em>Freaks and Geeks</em>, and has had cameo appearances on&nbsp;<em>The West Wing</em>&nbsp;and&nbsp;<em>Arrested Development</em>. For six years, he toured with the original cast of MST3K in a live movie riffing show called CINEMATIC TITANIC.</p><p style="text-align:justify"><strong>For press inquiries</strong>: chris@dumb-industries.com</p>'
					} imagesMaxWidth={Dimensions.get('window').width} />
				</View>
			</HeaderImageScrollView>
			<View style={{borderTopColor:'#ccc', borderTopWidth:1, padding:15}}>
				<Button
					title="Tickets"
					buttonStyle={{backgroundColor:BrandColor()}}
				/>
			</View>
		</View>
  );
}

export default Detail;