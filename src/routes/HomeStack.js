import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './../screens/home/Home';
import Detail from './../screens/home/Detail';

const Stack = createStackNavigator();
Stack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

function HomeStack() {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: false
      }}>
      <Stack.Screen
        name="Home"
        component={Home}/>
      <Stack.Screen
        name="Detail"
        component={Detail}/>
    </Stack.Navigator>
  );
}

export default HomeStack;