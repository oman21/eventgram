import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import Blank from './../screens/Blank';

const Stack = createStackNavigator();
Stack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

function BlankStack() {
  return (
    <Stack.Navigator
      initialRouteName="Blank"
      screenOptions={{
        headerShown: false
      }}>
      <Stack.Screen
        name="Blank"
        component={Blank}/>
    </Stack.Navigator>
  );
}

export default BlankStack;