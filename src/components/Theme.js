import React from 'react';
import { Text } from 'react-native';

export const EventText = (props) =>{
	return (
		<Text {...props} style={[{fontFamily: 'Nunito-Regular'}, props.style]}>{props.children}</Text>
	)
}

export const EventTextBold = (props) =>{
	return (
		<Text {...props} style={[{fontFamily: 'Nunito-ExtraBold'}, props.style]}>{props.children}</Text>
	)
}