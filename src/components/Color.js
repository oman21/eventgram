const brandColor = '#098cf6';
const dangerColor = '#f75e35';

export const BrandColor = () =>{
	return brandColor;
}

export const DangerColor = () =>{
	return dangerColor;
}